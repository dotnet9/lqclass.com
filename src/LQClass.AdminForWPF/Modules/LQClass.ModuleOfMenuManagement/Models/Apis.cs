﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LQClass.ModuleOfMenuManagement.Models
{       
    public static class Apis
    {
        public const string Search = "_FrameworkMenu/Search";
        public const string MenuId = "_FrameworkMenu/";
        public const string AddMenu = "_FrameworkMenu/Add";
        public const string GetFolders = "_FrameworkMenu/GetFolders";
    }
}
